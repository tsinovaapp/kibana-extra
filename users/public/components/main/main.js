import React from 'react';
import ReactTable from 'react-table';
import 'react-table/react-table.css';

import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';

import {
  EuiPage,
  EuiTitle,
  EuiPageBody,
  EuiPageContent,
  EuiPageContentHeader,
  EuiPageContentBody
} from '@elastic/eui';

export class Main extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    	usersList: null,
      modal: false
    };

    this.showConfirm = this.showConfirm.bind(this);
    this.fetchUsers = this.fetchUsers.bind(this);
  }

  showConfirm(userId) {

    confirmAlert({
      title: 'Excluir este usuário?',
      message: 'Você não será capaz de desfazer isso.',
      buttons: [
        {
          label: 'Excluir',
          onClick: () => this.deleteUser(userId)
        },
        {
          label: 'Cancelar',
          onClick: () => {}
        }
      ]
    });
  }

/*
    confirmAlert({
      customUI: ({ onClose }) => {
        return (
          <div className="custom-ui">
            <h1>Tem certeza?</h1>
            <p>Deseja deletar este usuário:</p>
            <button onClick={onClose}>Cancelar</button>
            <button onClick={() => { this.deleteUser(userId); }}>Sim, excluir</button>
          </div>
        );
      }
    });
*/
  /*
    const confirmDelete = confirm('Deseja realmente excluir o usuário?');
    if (confirmDelete) {
      //chama metodo de exclusão do servidor
      this.deleteUser(userId);
      //chama o metodo que atualiza a listagem removendo o item da tabela
      console.log('Id', userId);
    }
    // this.setState({
    //   modal: !this.state.modal
    // });
    // alert('teste');
  }
  */

  componentWillMount() {
    this.fetchUsers();
  }

  fetchUsers() {
    const { httpClient } = this.props;
    httpClient.get('../api/users/list')
      .then(response => {
        console.log(response);
        const users = response.data.map((user, key) => {
          const  id = user._id;
          const { name, login: username, email } = user._source;
          this.setState({ [id]: { id } });
          return {
            id,
            index: key,
            name,
            username,
            email,
            actions: this.showConfirm
          };
        });
        this.setState({ usersList: users });
      })
      .catch(err => {
        console.log(err);
      })
  }

  deleteUser(userId) {
    const { httpClient } = this.props;
    httpClient.delete(`../api/users/delete/${userId}`)
      .then(response => {
        console.log(response);
        const users = this.state.usersList.filter(user => {
          return user.id !== userId;
        });
        console.log(users);
        //remover o usuário com este ID da lista
        this.setState({ usersList: users });
      })
      .catch(err => {
        console.log(err);
      });
  }


  renderTable() {
    if (this.state.usersList) {
      return (
        <ReactTable
          data={this.state.usersList}
          columns={columns}
          defaultPageSize={10}
          minRows={10}
          className="-striped -highlight"
          previousText="Anterior"
          nextText="Próximo"
          loadingText="Carregando..."
          noDataText="Nenhum resultado encontrado"
          pageText="Página"
          ofPage="de"
          rowsText="linhas"
        />
      );
    }
    return <div style={{ width: '100%', textAlign: 'center', marginTop: '30px', minHeight: '200px' }}>Carregando...</div>;
  }

  render() {
    const { title } = this.props;
    return (
      <EuiPage>
        <EuiPageBody>
          <EuiPageContent>
            <EuiPageContentHeader>
              <EuiTitle>
                <h2>Usuários</h2>
              </EuiTitle>
              <a href="cadastrar"><button type="button" className="btn btn-primary btn-lg" style={{ width: '100%', fontWeight: 'bold', background: '#ef5039', borderColor: 'rgb(230, 90, 64)' }}>Novo Usuário</button></a>
            </EuiPageContentHeader>
            <EuiPageContentBody>
              <div className="table-responsive">
                { this.renderTable() }
              </div>
              <br/> <br/>        
            </EuiPageContentBody>
          </EuiPageContent>
        </EuiPageBody>
      </EuiPage>
    );
  }
}

const columns = [
  {
    Header: '#',
    accessor: 'index',
    resizable: true,
    width: 100,
    filterable: true
  },
  {
    Header: 'Nome',
    accessor: 'name',
    resizable: true,
    filterable: true
  },
  {
    Header: 'Username',
    accessor: 'username',
    resizable: true,
    filterable: true
  },
  {
    Header: 'E-mail',
    accessor: 'email',
    resizable: true,
    filterable: true
  },
  {
    Header: 'Ações',
    accessor: 'actions',
    width: 200,
    resizable: true,
    Cell: ({ value, original }) => {
      return (
        <div style={{ margin: 'auto', textAlign: 'center' }}>
          <a href={`edit_user?id=${original.id}`}>
            <img kbn-src="/plugins/users/edit.svg" aria-hidden="true" src="../plugins/users/edit.svg" style={{ width: '12%', marginRight: '30px' }} id="editar" data-toggle="tooltip" data-placement="top" title="Editar"/>
          </a>
          <img onClick={() => value(original.id)} kbn-src="/plugins/users/remove.svg" aria-hidden="true" src="../plugins/users/remove.svg" style={{ width: '12%' }} id="remover" data-toggle="tooltip" data-placement="top" title="Excluir" />        
        </div>
      );
    }
  }
];