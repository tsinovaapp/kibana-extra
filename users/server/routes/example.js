export default function (server) {

  server.route({
    path: '/api/users/example',
    method: 'GET',
    handler(req, reply) {
      reply({ time: (new Date()).toISOString() });
    }
  });



  server.route( {
    path: '/api/users/delete/{id}',
    method: 'DELETE',
    handler(req, reply) {

      var id = req.params.id;
      const dataCluster = server.plugins.elasticsearch.getCluster('data');

      dataCluster.callWithRequest(req, 'transport.request', {
          path: '/users/_doc/' + id,
          method: 'DELETE'
        }).then(res => {
          if ((res.result == "deleted" || res.result == "noop") && res._shards.failed == 0){
             reply({"success": true, "message": "Usuário deletado com sucesso!"});
           }else{
             reply({"success": false, "message": "Falha ao deletar o usuário!"});
           }

        });

      }

  });



  server.route({
   path: '/api/users/list',
   method: 'GET',
   handler(req, reply) {
     const dataCluster = server.plugins.elasticsearch.getCluster('data');
     dataCluster.callWithRequest(req, 'search', {
       index: "users",
       body: {
          "_source": {
              "exclude": [
                  "password"
              ]
          }
      }
     }).then(res => {
       reply(res.hits.hits);
     });

   }
 });




/*server.route({
   path: '/api/users/add',
   method: 'POST',
   handler(req, reply) {

     if (req.payload.name == undefined || req.payload.login == undefined
     || req.payload.password == undefined || req.payload.email == undefined){
       reply({"success": false, "message": "Informar o nome, login, senha e email."});
       return;
     }

     const dataCluster = server.plugins.elasticsearch.getCluster('data');

     dataCluster.callWithRequest(req, 'transport.request', {
       path: '/users/_doc/',
       method: 'POST',
       body: req.payload
     }).then(res2 => {

      if ((res2.result == "created" || res2.result == "noop") && res2._shards.failed == 0){
         reply({"success": true, "message": "Usuário cadastrado com sucesso!"});
       }else{
         reply({"success": false, "message": "Falha ao cadastrar o usuário!"});
       }

     });

   }

 });*/


 /*server.route({
   path: '/api/users/update',
   method: 'PUT',
   handler(req, reply) {

     if (req.payload.name == undefined || req.payload.login == undefined
     || req.payload.email == undefined){
       reply({"success": false, "message": "Informar o nome, login e email!"});
       return;
     }

     if (req.payload.id == undefined || req.payload.id == ""){
       reply({"success": false, "message": "Não foi informado o id do usuário!"});
       return;
     }

     if (req.payload.password != undefined && req.payload.password == ""){
       reply({"success": false, "message": "Não foi informada a senha!"});
       return;
     }

     var data;

     // foi informada uma nova senha
     if (req.payload.password != undefined){
       data = {
         "doc": {
           "email": req.payload.email,
           "name": req.payload.name,
           "login": req.payload.login,
           "password": req.payload.password
         }
       };
     }else{
       data = {
         "doc": {
           "email": req.payload.email,
           "name": req.payload.name,
           "login": req.payload.login
         }
       };
     }

     const dataCluster = server.plugins.elasticsearch.getCluster('data');

     dataCluster.callWithRequest(req, 'transport.request', {
       path: '/users/_doc/'+req.payload.id+'/_update',
       method: 'POST',
       body: data
     }).then(res2 => {
      if ((res2.result == "updated" || res2.result == "noop") && res2._shards.failed == 0){
         reply({"success": true, "message": "Cadastro atualizado com sucesso!"});
       }else{
         reply({"success": false, "message": "Falha ao atualizar o cadastro!"});
       }

     });

   }

 });*/


}
