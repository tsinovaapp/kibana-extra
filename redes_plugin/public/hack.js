import $ from 'jquery';

$(() => {
	const $appSwitcher = $('app-switcher');
	function moveTab(tabLabel) {
		$appSwitcher.find(`.global-nav-link:contains('${tabLabel}')`).appendTo($appSwitcher);
	}

	['Início', 'Redes', 'CFTV', 'SCA', 'Nobreak', 'Enfermaria', 'Cabeamento', 'Módulo de Análise', 'Machine Learning', 'Relatórios', 'Configurações', 'Alarmes e Logs', 'Ferramentas Nativas', 'Dashboard', 'Visualize', 'Discover', 'Management', 'Timelion', 'Dev Tools', 'APM', 'Monitoring', 'Sair'].forEach((label) => moveTab(label));

	function hide(tabLabel){
		$appSwitcher.find(`.global-nav-link:contains('${tabLabel}')`).remove();
	}

	['Dashboard', 'Visualize', 'Discover', 'Management', 'Timelion', 'Dev Tools', 'APM', 'Monitoring', 'Canvas', 'ElastAlert'].forEach((label) => hide(label));

});


/*$(document.body).on('keypress', function (event) {
  if (event.which === 13) {
    alert('boo!');
  }
});*/
