import $ from 'jquery';

$(() => {
	const $appSwitcher = $('app-switcher');
	function moveTab(tabLabel) {
		$appSwitcher.find(`.global-nav-link:contains('${tabLabel}')`).appendTo($appSwitcher);
	}
	['Sistemas','Dashboard','Visualize','Discover','Management', 'Timelion','Dev Tools'].forEach((label) => moveTab(label));

	function hide(tabLabel){
		$appSwitcher.find(`.global-nav-link:contains('${tabLabel}')`).remove();
	}
	['Discover','Management', 'Timelion','Dev Tools'].forEach((label) => hide(label));

});

/*
$(document.body).on('keypress', function (event) {
  if (event.which === 58) {
    alert('boo!');
  }
});
*/