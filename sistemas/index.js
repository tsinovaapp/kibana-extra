import exampleRoute from './server/routes/example';

export default function (kibana) {
  return new kibana.Plugin({
    require: ['elasticsearch'],
    name: 'sistemas',
    uiExports: {
      app: {
        title: 'Sistemas',  
        description: 'Sistemas Cubee',
        main: 'plugins/sistemas/app',
        styleSheetPath: require('path').resolve(__dirname, 'public/app.scss'),
        url: '/app/kibana#/dashboards',
        icon: 'pĺugins/sistemas/public/pie.svg',
      },
      hacks: [
        'plugins/sistemas/hack'
      ]
    },

    config(Joi) {
      return Joi.object({
        enabled: Joi.boolean().default(true),
      }).default();
    },

    init(server, options) { // eslint-disable-line no-unused-vars
      // Add server routes and initialize the plugin here
      exampleRoute(server);
    }
  });
}
