import exampleRoute from './server/routes/example';

export default function (kibana) {
  return new kibana.Plugin({
    require: ['elasticsearch'],
    name: 'alarmes_plugin',
    uiExports: {
      app: {
        title: 'Alarmes e Logs',
        description: 'Alarmes e logs',
        main: 'plugins/alarmes_plugin/app',
        styleSheetPath: require('path').resolve(__dirname, 'public/app.scss'),
        url: "/app/kibana#/dashboard/alarmes",
        icon: 'plugins/alarmes_plugin/icon.svg',
      },
    },

    config(Joi) {
      return Joi.object({
        enabled: Joi.boolean().default(true),
      }).default();
    },

    init(server, options) { // eslint-disable-line no-unused-vars
      // Add server routes and initialize the plugin here
      exampleRoute(server);
    }
  });
}
