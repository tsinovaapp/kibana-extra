export default function (server) {

  server.route({
    path: '/api/loginapp/example',
    method: 'GET',
    handler(req, reply) {
      reply({ time: (new Date()).toISOString() });
    }
  });

   server.route({
    path: '/api/loginapp/authentication',
    method: 'POST',
    handler(req, reply) {

      const dataCluster = server.plugins.elasticsearch.getCluster('data');
      dataCluster.callWithRequest(req, 'search', {
        index: 'users',
        q:"email:\""+req.payload.email+"\" AND password:\""+req.payload.password+"\""
      }).then(res => {
        reply(res.hits.hits);
      });

    }
  });

}
