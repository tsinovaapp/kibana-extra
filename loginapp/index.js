import exampleRoute from './server/routes/example';

export default function (kibana) {
  return new kibana.Plugin({
    require: ['elasticsearch'],
    name: 'loginapp',
    uiExports: {
      app: {
        title: 'Sair',
        description: 'Login do Cubee',
        main: 'plugins/loginapp/app',
        styleSheetPath: require('path').resolve(__dirname, 'public/app.scss'),
        icon: 'plugins/loginapp/icon.svg',
      },
      hacks: [
        'plugins/loginapp/hack'
      ]
    },

    config(Joi) {
      return Joi.object({
        enabled: Joi.boolean().default(true),
      }).default();
    },

    init(server, options) { // eslint-disable-line no-unused-vars
      // Add server routes and initialize the plugin here
      exampleRoute(server);
    }
  });
}
