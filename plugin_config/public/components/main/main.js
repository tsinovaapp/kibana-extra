import React from 'react';
import Select from 'react-select';
import JSONInput from 'react-json-editor-ajrm';
import locale    from 'react-json-editor-ajrm/locale/en';


import {
  EuiPage,
  EuiPageHeader,
  EuiTitle,
  EuiPageBody,
  EuiPageContent,
  EuiPageContentBody
} from '@elastic/eui';

export class Main extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    /*
       FOR EXAMPLE PURPOSES ONLY.  There are much better ways to
       manage state and update your UI than this.
    */
    const { httpClient } = this.props;
    httpClient.get('../api/pluginConfig/example').then((resp) => {
      this.setState({ time: resp.data.time });
    });
  }
  render() {
    const { title } = this.props;
    return (
      <EuiPage>
        <EuiPageBody>
          <EuiPageHeader>
            <EuiTitle size="l">
              <h1>Arquivo de Configuração de Plugin</h1>
            </EuiTitle>
          </EuiPageHeader>
          <EuiPageContent>
            <EuiPageContentBody>
            <div style={{ marginBottom: '50px', display: 'Flex', flexDirection: 'row', width: '500px', justifyContent: 'spaceBetween', alignItems: 'center' }}>
              <div style={{ fontWeight: 'bold' }}>Selecione o Plugin: </div>
              <div style={{ minWidth: '280px', marginLeft: '20px' }}>
                <Select
                  className="basic-single"
                  classNamePrefix="select"
                  defaultValue={null}
                  isDisabled={false}
                  isLoading={false}
                  isClearable={false}
                  isSearchable={false}
                  name="color"
                  options={pluginOptions}
               />
              </div>
            </div>

              <div style={{ marginBottom: '50px', display: 'Flex', flexDirection: 'row', justifyContent: 'spaceBetween' }}>
                <div style={{ width: '45%', marginRight: '50px' }}>
                  <table class="table table-striped table-responsive table-hover table-bordered">
                    <thead>
                      <tr>
                        <th> ID </th>
                        <th>Plugin</th>
                        <th>Configuração</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th>125</th>
                        <td>Snmpts</td>
                        <td>OK</td>
                      </tr>
                       <tr>
                        <th>185</th>
                        <td>VSpherets</td>
                        <td>Default</td>
                      </tr>
                       <tr>
                        <th>192</th>
                        <td>Redfishts</td>
                        <td>Default</td>
                      </tr>
                       <tr>
                        <th>592</th>
                        <td>Nobreakts</td>
                        <td>OK</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <div style={{ width: '50%' }}>
                  <JSONInput
                    id          = 'a_unique_id'
                    placeholder = { myObj }
                    locale      = { locale }
                    height      = '550px'
                 />
                </div>
              </div>
            </EuiPageContentBody>
          </EuiPageContent>
        </EuiPageBody>
      </EuiPage>
    );
  }
}
const pluginOptions = [
  { value: 'snmpts', label: 'Snmpts' },
  { value: 'vspherets', label: 'VSpherets' },
  { value: 'redfishts', label: 'Redfishts' },
  { value: 'nobreakts', label: 'Nobreakts' }
];

const myObj = {
  "name":"John",
  "age":30,
  "cars": [
    { "name":"Ford", "models":[ "Fiesta", "Focus", "Mustang" ] },
    { "name":"BMW", "models":[ "320", "X3", "X5" ] },
    { "name":"Fiat", "models":[ "500", "Panda" ] }
  ]
 }