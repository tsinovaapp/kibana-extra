import exampleRoute from './server/routes/example';

export default function (kibana) {
  return new kibana.Plugin({
    require: ['elasticsearch'],
    name: 'nobreak_plugin',
    uiExports: {
      app: {
        title: 'Nobreak',
        description: 'Monitoramento de Baterias',
        main: 'plugins/nobreak_plugin/app',
        styleSheetPath: require('path').resolve(__dirname, 'public/app.scss'),
        url: "/app/kibana#/dashboard/nobreak",
        icon: 'plugins/nobreak_plugin/icon.svg',
      },
    },

    config(Joi) {
      return Joi.object({
        enabled: Joi.boolean().default(true),
      }).default();
    },

    init(server, options) { // eslint-disable-line no-unused-vars
      // Add server routes and initialize the plugin here
      exampleRoute(server);
    }
  });
}
