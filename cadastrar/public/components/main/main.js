import React from 'react';
import { AvForm, AvField } from 'availity-reactstrap-validation';
import { Button, UncontrolledAlert } from 'reactstrap';

const md5 = require('md5');

import {
  EuiPage,
  EuiTitle,
  EuiPageBody,
  EuiPageContent,
  EuiPageContentHeader,
  EuiPageContentBody,
} from '@elastic/eui';

export class Main extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showMessage: false
    };
    this.handleValidSubmit = this.handleValidSubmit.bind(this);
  }


  handleValidSubmit(event, values) {
    this.setState({ values });
    const { httpClient } = this.props;
    const { name, username: login, email, originalPassword } = values;
    httpClient.post('../api/users/add', { name, login, email, password: md5(originalPassword) })
      .then(response => {
        console.log(response);
        if (response.data.success) {
          this.setState({ showMessage: true, status: 'success', message: 'Usuário cadastrado com sucesso.' });
        } else {
          this.setState({ showMessage: true, status: 'err', message: response.data.message });
        }
        this.form && this.form.reset();
      })
      .catch(err => {
        console.log(err);
        this.setState({ showMessage: true, status: 'err', message: 'Falha ao cadastrar o usuário.' });
      });
  }

  showMessage(message, status) {
    if (this.state.showMessage) {
      if (this.state.status === 'success') {
        return  <UncontrolledAlert color="success" fade={false} style={{ color: '#155724', backgroundColor: '#d4edda', borderColor: '#c3e6cb' }}>{ this.state.message }</UncontrolledAlert>;
      } else {
        return <UncontrolledAlert color="danger" fade={false} style={{ color: '#721c24', backgroundColor: '#f8d7da', borderColor: '#f5c6cb' }}>{ this.state.message }</UncontrolledAlert>;
      }
    }
  }

  render() {
    const { title } = this.props;
    return (
      <EuiPage>
        <EuiPageBody>
          <EuiPageContent>
            <EuiPageContentHeader>
              <EuiTitle>
                <h2>Cadastro de Usuário</h2>
              </EuiTitle>
            </EuiPageContentHeader>
            <EuiPageContentBody>

              {  this.showMessage() }

              <br/><br/>
              <div className="form-group">
                <div id="cadastro" style={{ display: 'flex', justifyContent: 'center' }}>
                  <AvForm  style={{  minWidth: '800px'  }} onValidSubmit={this.handleValidSubmit} ref={c => (this.form = c)}>
                    <div className="col-md-6 col-lg-6">
                      <AvField name="name" label="Nome:" type="text" required  errorMessage="Por favor, informe o nome completo."/>
                      <br/>
                      <AvField
                        name="username"
                        label="Username:"
                        validate={{ required: { value: true, errorMessage: 'Por favor, informe o username' },
                          pattern: { value: '^[A-Za-z0-9]+$', errorMessage: 'O username deve conter apenas letras e números.'},
                          minLength: { value: 6, errorMessage: 'O username deve conter entre 6 e 16 caracteres.' },
                          maxLength: { value: 16, errorMessage: 'O username deve conter entre 6 e 16 caracteres.' }
                        }} />
                     <br/>
                      <AvField
                        name="email"
                        label="E-mail:"
                        type="email"
                        validate={{ email: { value: true, errorMessage: 'Este não é um formato de email válido.' },
                          required: { value: true, errorMessage: 'Por favor, informe o email.' } }}
                      />
                    </div>

                    <div className="col-md-6 col-lg-6">
                      <AvField
                        name="originalPassword"
                        label="Senha:"
                        type="password"
                        validate={{ required: { value: true, errorMessage: 'Por favor, informe a senha.' },
                          minLength: { value: 6, errorMessage: 'A senha deve conter no mínimo 6 caracteres.' },
                          pattern: { value: '^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z])', errorMessage: 'A senha deve ser segura, conter pelo menos uma letra maíuscula, uma letra minúscula, um número e um caracter especial.' } }}
                      />
                      <br/>
                      <AvField
                        name="confirmationPassword"
                        label="Confirme a senha:"
                        type="password"
                        validate={{ match: { value: 'originalPassword', errorMessage: 'A senha não confere.' },
                          required: { value: true, errorMessage: 'Por favor, confirme a senha.' } }}
                      />
                      <br/>
                      <UncontrolledAlert color="warning" fade={false} style={{ color: '#856404', backgroundColor: '#fff3cd', borderColor: '#ffeeba' }}>
                        <b>Dica:</b> A senha deve conter no mínimo: <br/><br/>
                          - Seis dígitos;<br/>
                          - Uma letra maíscula;<br/>
                          - Uma letra minúscula;<br/>
                          - Um número;<br/>
                          - Um caracter especial (Ex: !,@, #).<br/>
                      </UncontrolledAlert>

                    </div>
                    <Button className="btn btn-lg btn-block" onSubmit={this.handleValidSubmit.bind(this)} style={{ color: 'white', width: '100%', background: '#ef5039', borderColor: 'rgb(230, 90, 64)', fontWeight: 'bold' }}>Cadastrar</Button>
                  </AvForm>
                </div>
              </div>
            </EuiPageContentBody>
          </EuiPageContent>
        </EuiPageBody>
      </EuiPage>
    );
  }
}


