export default function (server) {

  server.route({
    path: '/api/devices/example',
    method: 'GET',
    handler(req, reply) {
      reply({ time: (new Date()).toISOString() });
    }
  });


  server.route( {
    path: '/api/devices/delete/category',
    method: 'PUT',
    handler(req, reply) {

      var macaddress = req.payload.macaddress;

      const dataCluster = server.plugins.elasticsearch.getCluster('data');

      dataCluster.callWithRequest(req, 'search', {
        index: "snmpts*",
        body: {
    "query": {
        "match": {
            "device.mac.keyword": {
                "query": macaddress
            }
        }
    },
    "size": 1,
    "sort": [
        {
            "@timestamp": {
                "order": "desc"
            }
        }
    ]
}

      }).then(res => {

        var dataDevice = res.hits.hits[0];

        dataCluster.callWithRequest(req, 'transport.request', {
            path: '/' + dataDevice._index + '/doc/' + dataDevice._id + "/_update",
            method: 'POST',
            body: {
              "script": "ctx._source.device.remove(\"category\")"
            }
          }).then(res2 => {

            if ((res2.result == "updated" || res2.result == "noop") && res2._shards.failed == 0){
               reply({"success": true, "message": "Categoria removida com sucesso!"});
             }else{
               reply({"success": false, "message": "Falha ao tentar remover a categoria!"});
             }

          });

      });

      }

  });


  server.route( {
    path: '/api/devices/delete/systems',
    method: 'PUT',
    handler(req, reply) {

      var macaddress = req.payload.macaddress;

      const dataCluster = server.plugins.elasticsearch.getCluster('data');

      dataCluster.callWithRequest(req, 'search', {
        index: "snmpts*",
        body: {
    "query": {
        "match": {
            "device.mac.keyword": {
                "query": macaddress
            }
        }
    },
    "size": 1,
    "sort": [
        {
            "@timestamp": {
                "order": "desc"
            }
        }
    ]
}

      }).then(res => {

        var dataDevice = res.hits.hits[0];

        dataCluster.callWithRequest(req, 'transport.request', {
            path: '/' + dataDevice._index + '/doc/' + dataDevice._id + "/_update",
            method: 'POST',
            body: {
              "script": "ctx._source.device.remove(\"systems\")"
            }
          }).then(res2 => {

            if ((res2.result == "updated" || res2.result == "noop") && res2._shards.failed == 0){
               reply({"success": true, "message": "Sistemas removido com sucesso!"});
             }else{
               reply({"success": false, "message": "Falha ao tentar remover os sistemas!"});
             }

          });

      });

      }

  });

  server.route({
    path: '/api/devices/update',
    method: 'PUT',
    handler(req, reply) {

      var macaddress = req.payload.macaddress;
      var category = req.payload.category;
      var systems = req.payload.systems;

      if (category == undefined && systems == undefined){
        reply({"success": false, "message": "Informar a categoria e/ou o sistema do dispositivo"});
        return;
      }

      const dataCluster = server.plugins.elasticsearch.getCluster('data');

      dataCluster.callWithRequest(req, 'search', {
        index: "snmpts*",
        body: {
    "query": {
        "match": {
            "device.mac.keyword": {
                "query": macaddress
            }
        }
    },
    "size": 1,
    "sort": [
        {
            "@timestamp": {
                "order": "desc"
            }
        }
    ]
}

      }).then(res => {

        var dataDevice = res.hits.hits[0];

        var dataDeviceUpdate;

        if (category == undefined && systems == undefined){
          dataDeviceUpdate = null;

        }else if (category != undefined && systems == undefined){
          dataDeviceUpdate = {
              "doc": {
                  "device": {
                      "category": category
                  }
              }
          };

        }else if (category == undefined && systems != undefined){
          dataDeviceUpdate = {
              "doc": {
                  "device": {
                      "systems": systems
                  }
              }
          };

        }else{
          dataDeviceUpdate = {
              "doc": {
                  "device": {
                      "category": category,
                      "systems": systems
                  }
              }
          };

        }

        if (dataDeviceUpdate == null){
          reply({"success": false, "message": "Informar a categoria e/ou o sistema do dispositivo"});

        }else{

          dataCluster.callWithRequest(req, 'transport.request', {
            path: '/' + dataDevice._index + '/doc/' + dataDevice._id + '/_update',
            method: 'POST',
            body: dataDeviceUpdate
          }).then(res2 => {

           if ((res2.result == "updated" || res2.result == "noop") && res2._shards.failed == 0){
              reply({"success": true, "message": "Informações do dispositivo atualizada com sucesso!"});
            }else{
              reply({"success": false, "message": "Falha ao atualizar os dados do dispositivo!"});
            }

          });

        }

      });

    }

  });



  server.route({
   path: '/api/devices/list',
   method: 'GET',
   handler(req, reply) {

     const dataCluster = server.plugins.elasticsearch.getCluster('data');
     dataCluster.callWithRequest(req, 'search', {
       index: "snmpts*",
       body: {
         "size": 0,
    "query": {
        "bool": {
            "should": [
                {
                    "exists": {
                        "field": "device.mac"
                    }
                }
            ]
        }
    },
    "aggs": {
        "group": {
            "terms": {
                "field": "device.mac.keyword",
                "size": 2147483647
            },
            "aggs": {
                "group_docs": {
                    "top_hits": {
                        "sort": [
                            {
                                "@timestamp": {
                                    "order": "desc"
                                }
                            }
                        ],
                        "size": 1
                    }
                }
            }
        }
    }

}
     }).then(res => {
       reply(res.aggregations.group.buckets);
     });

   }
 });


}
