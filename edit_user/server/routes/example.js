export default function (server) {

  server.route({
    path: '/api/editUser/example',
    method: 'GET',
    handler(req, reply) {
      reply({ time: (new Date()).toISOString() });
    }
  });


  /*server.route({
   path: '/api/users/list',
   method: 'GET',
   handler(req, reply) {
     const dataCluster = server.plugins.elasticsearch.getCluster('data');
     dataCluster.callWithRequest(req, 'search', {
       index: "users",
       body: {
          "_source": {
              "exclude": [
                  "password"
              ]
          }
      }
     }).then(res => {
       reply(res.hits.hits);
     });

   }
 });*/



 server.route({
   path: '/api/users/update',
   method: 'PUT',
   handler(req, reply) {

     if (req.payload.name == undefined || req.payload.login == undefined
     || req.payload.email == undefined){
       reply({"success": false, "message": "Informar o nome, login e email!"});
       return;
     }

     if (req.payload.id == undefined || req.payload.id == ""){
       reply({"success": false, "message": "Não foi informado o id do usuário!"});
       return;
     }

     if (req.payload.password != undefined && req.payload.password == ""){
       reply({"success": false, "message": "Não foi informada a senha!"});
       return;
     }

     var data;

     // foi informada uma nova senha
     if (req.payload.password != undefined){
       data = {
         "doc": {
           "email": req.payload.email,
           "name": req.payload.name,
           "login": req.payload.login,
           "password": req.payload.password
         }
       };
     }else{
       data = {
         "doc": {
           "email": req.payload.email,
           "name": req.payload.name,
           "login": req.payload.login
         }
       };
     }

     const dataCluster = server.plugins.elasticsearch.getCluster('data');

     dataCluster.callWithRequest(req, 'transport.request', {
       path: '/users/_doc/'+req.payload.id+'/_update',
       method: 'POST',
       body: data
     }).then(res2 => {
      if ((res2.result == "updated" || res2.result == "noop") && res2._shards.failed == 0){
         reply({"success": true, "message": "Cadastro atualizado com sucesso!"});
       }else{
         reply({"success": false, "message": "Falha ao atualizar o cadastro!"});
       }

     });

   }

 });

}
