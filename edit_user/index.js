import exampleRoute from './server/routes/example';

export default function (kibana) {
  return new kibana.Plugin({
    require: ['elasticsearch'],
    name: 'edit_user',
    uiExports: {
      app: {
        title: 'Edit User',
        description: 'Editar dados cadastrais do Usuário',
        main: 'plugins/edit_user/app',
      },
      hacks: [
        'plugins/edit_user/hack'
      ]
    },

    config(Joi) {
      return Joi.object({
        enabled: Joi.boolean().default(true),
      }).default();
    },

    init(server, options) { // eslint-disable-line no-unused-vars
      // Add server routes and initialize the plugin here
      exampleRoute(server);
    }
  });
}
