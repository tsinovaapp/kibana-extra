import React from 'react';
import { AvForm, AvField } from 'availity-reactstrap-validation';
import { Button, UncontrolledAlert } from 'reactstrap';

import md5 from 'md5';

import {
  EuiPage,
  EuiTitle,
  EuiPageBody,
  EuiPageContent,
  EuiPageContentHeader,
  EuiPageContentBody,
} from '@elastic/eui';

export class Main extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showMessage: false,
      userId: null, //'O5nCumgB-DEDVT8NplvF'
      getQueryVariable: null
    };

    this.handleValidSubmit = this.handleValidSubmit.bind(this);
    this.hasNewPassword = this.hasNewPassword.bind(this);
  }

  componentDidMount() {
    this.fetchUser(this.getQueryVariable('id'));
  }

  getQueryVariable(variable) {
    const query = window.location.search.substring(1);
    const vars = query.split('&');
    for (let i = 0; i < vars.length; i++) {
      const pair = vars[i].split('=');
      if(pair[0] === variable) { return pair[1]; }
    }
    return(false);
  }

  handleValidSubmit(event, values) {
    console.log(values);
    //this.setState({ values });
    const { httpClient } = this.props;
    const { name, login, email, newPassword } = values;
    httpClient.put('../api/users/update', { name, login, email, password: md5(newPassword), id: this.state.userData.id })
      .then(response => {
        console.log(response);
        if (response.data.success) {
          this.setState({ showMessage: true, status: 'success', message: 'As edições foram salvas com sucesso.' });
        } else {
          this.setState({ showMessage: true, status: 'err', message: 'Falha ao salvar edições do usuário.' });
        }
      })
      .catch(err => {
        console.log(err);
        this.setState({ showMessage: true, status: 'err', message: 'Falha ao salvar edições do usuário.' });
      });
  }

  showMessage(message, status) {
    if (this.state.showMessage) {
      if (this.state.status === 'success') {
        return  <UncontrolledAlert color="success" fade={false} style={{ color: '#155724', backgroundColor: '#d4edda', borderColor: '#c3e6cb' }}>{ this.state.message }</UncontrolledAlert>;
      }
        return <UncontrolledAlert color="danger" fade={false} style={{ color: '#721c24', backgroundColor: '#f8d7da', borderColor: '#f5c6cb' }}>{ this.state.message }</UncontrolledAlert>;
    }
  }

  hasNewPassword() {
    return this.password.FormCtrl.getValues().newPassword ? true : false;
  }

  fetchUser(userId) {
    const { httpClient } = this.props;
    httpClient.get('../api/users/list')
      .then(response => {
        console.log(response);
        const user = response.data.find((user) => {
          return user._id === userId;
        });
        if (user) {
          this.setState({ userData: { ...user._source, id: user._id } });
        } else {
          // console.log(user);
          window.location = 'users';
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  render() {
    const { title } = this.props;
    if (!this.state.userData) {
      return 'Carregando...';
    }

    return (
      <EuiPage>
        <EuiPageBody>
          <EuiPageContent>
            <EuiPageContentHeader>
              <EuiTitle>
                <h2>Editar Dados Cadastrais</h2>
              </EuiTitle>
            </EuiPageContentHeader>
            <EuiPageContentBody>
              {  this.showMessage() }
              <br/><br/>
              <div className="form-group">
                <div id="cadastro" style={{ display: 'flex', justifyContent: 'center' }}>

                  <AvForm  style={{  minWidth: '700px'  }} onValidSubmit={this.handleValidSubmit} model={this.state.userData}>
                    <div className="col-md-6 col-lg-6">
                      <AvField name="name" label="Nome:" type="text" required  errorMessage="Por favor, informe o nome completo."/>
                      <br/>
                      <AvField
                        name="login"
                        label="Username:"
                        validate={{
                         required: { value: true, errorMessage: 'Por favor, informe o username' },
                         pattern: { value: '^[A-Za-z0-9]+$', errorMessage: 'O username deve conter apenas letras e números.' },
                         minLength: { value: 6, errorMessage: 'O username deve conter entre 6 e 16 caracteres.' },
                         maxLength: { value: 16, errorMessage: 'O username deve conter entre 6 e 16 caracteres.' }
                      }} />
                      <br/>
                      <AvField
                        name="email"
                        label="E-mail:"
                        type="email"
                        validate={{
                         email: { value: true, errorMessage: 'Este não é um formato de email válido.' },
                         required: { value: true, errorMessage: 'Por favor, informe o email.' } }}
                       />
                    </div>

                    <div className="col-md-6 col-lg-6">
                       <AvField 
                        name="newPassword"
                        label="Redefinir Senha:"
                        ref={element => { this.password = element; }}
                        type="password"
                        validate={{
                          required: { value: false, errorMessage: 'Por favor, informe a senha.' },
                          minLength: { value: 6, errorMessage: 'A senha deve conter no mínimo 6 caracteres.' },
                          pattern: { value: '^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z])', errorMessage: 'A senha deve ser segura, conter pelo menos uma letra maíuscula, uma letra minúscula, um número e um caracter especial.' } }}
                        />
                      <br/>

                      <AvField
                        name="confirmationPassword"
                        label="Confirme a senha:"
                        type="password"
                        validate={{
                        match: { value: 'newPassword', errorMessage: 'A senha não confere.' },
                        required: { value: false, errorMessage: 'Por favor, confirme a senha.' } }}
                      />
                      <br/>
                      <UncontrolledAlert color="info" fade={false} style={{ color: '#0c5460', backgroundColor: '#d1ecf1', borderColor: '#bee5eb' }}>
                        Caso <b>não</b> queira redefinir a senha:<br/>
                        Não preencha os campos acima!<br/>
                      </UncontrolledAlert>       
                    </div>

                    <div className="col-md-12 col-lg-12">   
                     <UncontrolledAlert color="warning" fade={false} style={{ color: '#856404', backgroundColor: '#fff3cd', borderColor: '#ffeeba' }}>
                        <b>Dica:</b> A senha deve conter no mínimo: <br/><br/>
                            - Seis dígitos;<br/>
                            - Uma letra maíscula;<br/>
                            - Uma letra minúscula;<br/>
                            - Um número;<br/>
                            - Um caracter especial (Ex: !,@, #).<br/>
                      </UncontrolledAlert>
                      </div>
                    <Button className="btn btn-lg btn-block" onSubmit={this.handleValidSubmit} style={{ color: 'white', width: '100%', background: '#ef5039', borderColor: 'rgb(230, 90, 64)', fontWeight: 'bold' }}>Salvar alterações</Button>
                  </AvForm>
                </div>
              </div>

            </EuiPageContentBody>
          </EuiPageContent>
        </EuiPageBody>
      </EuiPage>
    );
  }
}


