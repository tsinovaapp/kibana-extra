import exampleRoute from './server/routes/example';

export default function (kibana) {
  return new kibana.Plugin({
    require: ['elasticsearch'],
    name: 'dashboard_plugin',
    uiExports: {
      app: {
        title: 'Início',
        description: 'Visão geral dos sistemas',
        main: 'plugins/dashboard_plugin/app',
        styleSheetPath: require('path').resolve(__dirname, 'public/app.scss'),
        url: "/app/kibana#/dashboard/inicio",
        icon: 'plugins/dashboard_plugin/icon.svg',
      },
      hacks: [
        'plugins/dashboard_plugin/hack'
      ]
    },

    config(Joi) {
      return Joi.object({
        enabled: Joi.boolean().default(true),
      }).default();
    },

    init(server, options) { // eslint-disable-line no-unused-vars
      // Add server routes and initialize the plugin here
      exampleRoute(server);
    }
  });
}
